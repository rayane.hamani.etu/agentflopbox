package agentflopbox;



import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.httpclient.HttpClient;



public class Agent
{
	private HttpClient client = new HttpClient();
	
	private String root = "root";
	private String bin  = ".deleted";
	
	private ArrayList<Server> servers = new ArrayList<Server>();
	
	public HttpClient        getClient()    { return this.client; }
	public ArrayList<Server> getServers()   { return this.servers; }
	
	
	
	public Agent()
	{}
	
	
	
	/**
	 * Create the directories for each registered server and the bin directory.
	 */
	public void createDirectories()
	{
		File bin = new File(this.bin);
		
		if(!bin.exists())
			bin.mkdirs();
		
		for(Server server : this.servers)
		{
			File dir = new File(this.root + server.getName());
			
			if(!dir.exists())
				dir.mkdirs();
			
		}
	}
	
	/**
	 * Import the data of the registered servers.
	 */
	public void importRemoteData()
	{
		for(Server server : this.servers)
		{
			Scanner scanner = new Scanner(AgentCommand.list(server));
			
			while(scanner.hasNext())
			{
				String file = scanner.nextLine();
				
				file = file.substring(file.lastIndexOf(" ")+1);
				
				AgentCommand.download(server, file);
			}
			
			scanner.close();
		}
	}
	
	/**
	 * Watch the local files and update the corresponding server when a local change occurs.
	 */
	public void updateServer()
	{
		try
		{
			WatchService watchService = FileSystems.getDefault().newWatchService();
			
			Path path = Paths.get("root");
			
			path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
			
			WatchKey watchKey;
			while((watchKey = watchService.take()) != null)
			{
				for(WatchEvent<?> event : watchKey.pollEvents())
				{
					String fileCreated = event.context().toString();
					
					Server server = AgentUtil.findServerByName(fileCreated.substring(0, fileCreated.indexOf("/")));
					
					String to = fileCreated.substring(fileCreated.indexOf("/")+1);
					
					if(event.kind().equals(StandardWatchEventKinds.ENTRY_DELETE) || event.kind().equals(StandardWatchEventKinds.ENTRY_MODIFY))
						AgentCommand.delete(server, to);
					
					if(event.kind().equals(StandardWatchEventKinds.ENTRY_CREATE) || event.kind().equals(StandardWatchEventKinds.ENTRY_MODIFY))
						AgentCommand.upload(server, root + "/" + fileCreated, to);
				}
			}
		}
		catch(IOException | InterruptedException e)
		{
			// do nothing
		}
	}
}
