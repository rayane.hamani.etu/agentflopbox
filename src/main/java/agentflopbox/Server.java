package agentflopbox;



public class Server
{
	private String name;
	private String port;
	private User   user;
	
	
	
	public Server(String name, String port, User user)
	{
		this.name = name;
		this.port = port;
		this.user = user;
	}
	
	public Server(String name, String port)
	{
		this.name = name;
		this.port = port;
		this.user = new User("anonymous", "anonymous");
	}
	
	public Server(String name)
	{
		this.name = name;
		this.port = "21";
		this.user = new User("anonymous", "anonymous");
	}
	
	
	
	public String getName() { return this.name; }
	public String getPort() { return this.port; }
	public User   getUser() { return this.user; }

	public void setPort(String port) { this.port = port; }
	public void setUser(User user)   { this.user = user; }
}
